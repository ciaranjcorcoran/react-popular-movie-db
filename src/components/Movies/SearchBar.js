import React, { Component } from 'react';
import Aux from '../../hoc/Aux';

class SearchBar extends Component {
  state = {
    term: ''
  }

  onInputChange(term) {
    this.setState({term});
    this.props.searchMovies(term);
  }

  render() {
    return (
			<Aux>
				<div>Search filter: </div>
				<input type="text"
					value={this.state.term}
					onChange={event => this.onInputChange(event.target.value)}
				/>
			</Aux>
    );
  }
}

export default SearchBar;