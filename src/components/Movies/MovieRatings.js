import React from 'react';
import Aux from '../../hoc/Aux';

const movieRatings = (props) => {
  const ratings = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10];

  // Map ratings array to buttons
  const buttons = ratings.map(res => {
    return (
      <div className="col-3" key={res}>
        <button className="btn btn-primary"
          id={res}
          onClick={event => props.getSelected(event.target.value)}
          value={res}>{res}</button>
      </div>
    );
  });
  return (
    <Aux>
      <div className="row">
        <h2>Select a rating to filter:</h2>
      </div>
      <div className="row">
        {buttons}
      </div>
    </Aux>
  )
}

export default movieRatings;