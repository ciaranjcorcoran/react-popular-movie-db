import React from 'react';

const movieGenres = (props) => {
	return (
		<div className="col-3">
			<input 
				type="checkbox"
				id={props.id}
				value={props.genreName}
				onChange={event => props.getSelected(event.target.id) } />
			<label htmlFor={props.genreName}>{props.genreName}</label>
		</div>
	);
}

export default movieGenres;