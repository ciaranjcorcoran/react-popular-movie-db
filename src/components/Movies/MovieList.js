import React from 'react';

const movieList = (props) => {
  return (
    <div className="col-6">
      <h2>{props.movieName}</h2>
      <img src={props.movieImg} alt={props.movieName} />
    </div>
  );
}

export default movieList;