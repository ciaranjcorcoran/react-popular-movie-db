import React, { Component } from 'react';
import MovieList from './MovieList';

class Movies extends Component {
  render() {

    // Sort movie list by populirty then map to Movie List component
    const moviesList = this.props.filteredMovies.sort((a, b) => b.popularity - a.popularity).map(mov => {
      return <MovieList
        key={mov.id}
        movieName={mov.title}
        movieImg={'https://image.tmdb.org/t/p/w200' + mov.poster_path}
        />
    });

    return (
      <div className="row">
        {moviesList}
      </div>
    )
  }
}

export default Movies;