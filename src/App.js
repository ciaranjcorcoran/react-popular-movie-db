import React, { Component } from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Movies from './components/Movies/Movies';
import MovieRatings from './components/Movies/MovieRatings';
import MovieGenres from './components/Movies/MovieGenres';
import SearchBar from './components/Movies/SearchBar';

const API_KEY = '2234cacce9b0984db6e042121f89f906';
const selectedGenres = [];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movies: [],
      filteredMovies: [],
      genres: []
    };
  }

  componentDidMount() {
    let url = `https://api.themoviedb.org/3/movie/now_playing?api_key=${API_KEY}&language=en-US&page=1`;
    let genresUrl = `https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=en-US`;

    fetch(url).then(response => {
      return response.json();
    }).then(data => {
      this.setState({
        movies: data.results,
        filteredMovies: data.results
      });
    });

    fetch(genresUrl).then(response => {
      return response.json();
    }).then(data => {
      this.setState({
        genres: data.genres
      });
    });
  }

  handleRatingChecked(e) {
    // filter by movie rating
    const filteredRatings = this.state.movies.filter(result => {
      return result.vote_average >= e;
    });
    
    this.setState({
      filteredMovies: filteredRatings
    });
  }
  
  // filter selected movie once
  handleGenreFilter(e) {
    let eValue = parseInt(e, 10);
    
    if(!selectedGenres.includes(eValue)) {
			selectedGenres.push(eValue);
    }

    // Filter genres that contain one of the selected genre ids 
    let newFilteredGenres = this.state.movies.filter(res => {
      return res.genre_ids.some(r => selectedGenres.includes(r));
    });

		this.setState({
      filteredMovies: newFilteredGenres
    });
  }

  // Search list functionality
  movieSearch(term) {
    let results = this.state.movies.filter(result => {
      return result.title.toLowerCase().indexOf(term.toLowerCase()) !== -1;
    });
    this.setState({filteredMovies: results});
  }

  render() {
    // Filter genres to match only genres contained in movies list and map to movie genre const
    const ids = this.state.movies.map(mov => mov.genre_ids);
    const newIds = [].concat.apply([], ids);

    const filteredGenres = this.state.genres.filter(result => {
      return newIds.includes(result.id);
    });

    const movieGenres = filteredGenres.map(mov => {
      return <MovieGenres
        key={mov.id}
        id={mov.id}
        genreName={mov.name}
        getSelected={this.handleGenreFilter.bind(this)}
      />
    });

    return (
      <div className="container">
        <div className="row">
          <SearchBar searchMovies={this.movieSearch.bind(this)} />
        </div>
        <div className="row">
          <MovieRatings
            getSelected={this.handleRatingChecked.bind(this)} />
        </div>
        <div className="row">
          <h2>Select a genre to filter by:</h2>
        </div>
        <div className="row">
          {movieGenres}
        </div>
        <Movies
          filteredMovies={this.state.filteredMovies}
          filteredGenres={filteredGenres} />
      </div>
    );
  }
}

export default App;
